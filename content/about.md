+++
date = "2015-08-26T11:45:57-04:00"
draft = true
title = "about"

+++

Progressive Technology Project is a capacity building and technology services organization with deep roots in organized communities of color and low income communities across the United States. Our mission is to help community organizing groups achieve their goals more effectively and efficiently. We do that by helping them build their capacity — both skills and understanding — to integrate technology strategically into the life of their organizations. 

